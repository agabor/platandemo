/// <reference path="jquery.d.ts" />
/// <reference path='jqueryui.d.ts' />

declare var google;

class PieChart {

    chart: any;

    constructor(public data:any, public element: HTMLElement, public selectHandler: any)
    {
       this.chart = new google.visualization.PieChart(this.element);
    }

    drawChart() {
        var dataTable = google.visualization.arrayToDataTable(this.data);

        var options = {
            pieHole: 0.4,
            legend: {position: 'none'},
            chartArea: {width: '100%', height: '90%'},
            pieSliceTextStyle: {fontName: "Helvetica"},
            tooltip: {trigger: 'none'},
            pieSliceText: 'label'
        }

        this.chart.draw(dataTable, options);

        google.visualization.events.addListener(this.chart, 'select', this.selectHandler);

    }

    getSelectedIndex() : number
    {
        return this.chart.getSelection()[0].row;
    }
}

class MainScreen {
    chart: PieChart;
    element: HTMLElement
    constructor(public data:any, element: HTMLElement, selectHandler)
    {
        this.element = element;
        this.chart = new PieChart(data, document.getElementById('donutchart'), selectHandler);
        this.chart.drawChart();
        clearDataElement();
    }
}

class TableScreen {
    element: HTMLElement;
    table: HTMLTableElement;
    button: HTMLElement;
    constructor(data:Array<any[]>, title: string) {
        this.element = document.createElement("div");
        this.element.setAttribute("id", "tablescreen");
        this.element.setAttribute("class", "appscreen");

        var header  = document.createElement("div");
        header.setAttribute("id", "header");
        this.element.appendChild(header);

        this.button = document.createElement("button");
        this.button.setAttribute("class", "ios-6-arrow left blue");
        this.button.setAttribute("data-title", "Back");
        this.button.setAttribute("onClick", "setMainScreen()");
        header.appendChild(this.button)

        var titleText = document.createElement("span");
        titleText.setAttribute("id", "tabletitle");
        titleText.appendChild(document.createTextNode(title))
        header.appendChild(titleText)

        this.table = document.createElement("table");
        this.table.setAttribute("id", "expensetable")
        var row = this.addRow('Date', 'Payee', 'Amount');
        for (var i = 0; i < data.length; ++i) {
            var datarow = data[i];
            this.addRow("" + year + "." + (month + 1) + "." + datarow[0], datarow[1], datarow[2] + " €");
        }
        row.setAttribute("class", "header");
        this.element.appendChild(this.table)
    }

    addRow(val1: string, val2: string, val3: string) : HTMLElement{
        var row = this.table.insertRow();
        this.AddCell(row, val1);
        this.AddCell(row, val2);
        this.AddCell(row, val3);
        return row;
    }

    AddCell(row:HTMLElement, text: string) {
        var cell = document.createElement("td");
        cell.appendChild(document.createTextNode(text))
        row.appendChild(cell);
    }
}

var mainscreen:MainScreen;
var tablesceen:TableScreen;

function clearDataElement() {
    var dataElement = document.getElementById('data');
    while (dataElement.firstChild) {
        dataElement.removeChild(dataElement.firstChild);
    }
}
function selectHandler(e) {
    var data = mainscreen.data[mainscreen.chart.getSelectedIndex() + 1];
    clearDataElement();
    var dataElement = document.getElementById('data');
    var paragraph = document.createElement("p");
    paragraph.appendChild(document.createTextNode(data[0] + ": " + data[1] + " € "));
    var showBtn = document.createElement("span");
    showBtn.setAttribute("id", "showbtn");
    showBtn.setAttribute("onClick", "setTableScreen()");
    showBtn.appendChild(document.createTextNode("Show →"));
    paragraph.appendChild(showBtn);
    dataElement.appendChild(paragraph);
}

function getElement(list : string[]) :string
{
    var index : number = Math.floor(Math.random() * list.length);
    return list[index];
}

var firstyear: number = 2011;
var lastyear: number = 2013;
var year: number = 2013;
var month: number = 0;
var expenses : Array<Array<Array<any[]> > >;
var types: string[] = ['Food', 'Transportation', 'Clothes', 'Housing', 'Housing costs'];
var food: string[] = ["Edeka", "Aldi", "Lidl", "Netto", "InterSpar"];
var transportation: string[] = ["OMW", "Deutsche Bahn", "MOL", "BKV"];
var clothes: string[] = ["C & A", "H & M", "Charles Vögele", "Kaufhof"];
var housing: string[] = ["Frau Müller"];
var housingCosts: string[] = ["EON", "1 und 1"];
var shopLists: string[][] = [food, transportation, clothes, housing, housingCosts];

function generateExpenses(): Array<Array<any[]> > {
    var expenses: Array<Array<any[]> > = [[], [], [], [], []];

    var salary:number = 1000;
    while (salary > 0) {
        var type:number = Math.floor(Math.random() * shopLists.length);
        var amount:number = Math.floor(Math.random() * 100) / 2;
        var day:number = Math.floor(Math.random() * 28) + 1;
        expenses[type].push([day, getElement(shopLists[type]), amount]);
        salary -= amount;
    }
    return expenses;
}

function aggregateExpenses(expenses: Array<Array<any[]> >): any[] {
    var data:any[] = [
        ['Type', 'Euro in month']
    ];

    for (var i = 0; i < types.length; ++i) {
        var explist = expenses[i];
        explist.sort(function (a, b) {
            return a[0] - b[0]
        })
        var amount:number = 0;
        for (var j = 0; j < explist.length; ++j) {
            amount += explist[j][2];
        }
        data.push([types[i], amount]);
    }
    return data;
}

function main()
{
    expenses = [];
    var monthcount: number = (lastyear - firstyear + 1) * 12;
    for (var i = 0; i < monthcount; ++i)
       expenses[i] = generateExpenses();
    var data = aggregateExpenses(expenses[month]);
    mainscreen = new MainScreen(data, document.getElementById('mainscreen'), selectHandler);
}

function setMainScreen()
{
    var screen = document.getElementById('screen');
    screen.removeChild(tablesceen.element);
    screen.appendChild(mainscreen.element);
}

function setTableScreen()
{
    var index = mainscreen.chart.getSelectedIndex();
    tablesceen = new TableScreen(expenses[getMonthIndex()][index], types[index]);
    var screen = document.getElementById('screen');
    screen.removeChild(mainscreen.element);
    screen.appendChild(tablesceen.element);
}

function getMonthIndex() {
    return (year - firstyear) * 12 + month;
}

function refreshChart() {
    var data = aggregateExpenses(expenses[getMonthIndex()]);
    mainscreen = new MainScreen(data, document.getElementById('mainscreen'), selectHandler);
}

function setMonth(m: number)
{
    var monthBtn = document.getElementById('m' + month);
    monthBtn.removeAttribute("class");
    month = m;
    monthBtn = document.getElementById('m' + month);
    monthBtn.setAttribute("class", "selected");
    refreshChart();
}

function setYear() {
    var yearlabel:HTMLElement = document.getElementById("year");
    yearlabel.removeChild(yearlabel.firstChild);
    yearlabel.appendChild(document.createTextNode(""+year));
    refreshChart();
}

function deactivate(button: HTMLElement) {
    button.removeAttribute("onClick");
    button.setAttribute("class", "inactive");
}

function activate(button: HTMLElement, callback) {
    button.setAttribute("onClick", callback);
    button.setAttribute("class", "active");
}

function setRightArrow(button: HTMLElement) {
    var cl: string = button.getAttribute("class");
    button.setAttribute("class", "fa fa-chevron-right " + cl);
}

function setLeftArrow(button: HTMLElement) {
    var cl: string = button.getAttribute("class");
    button.setAttribute("class", "fa fa-chevron-left " + cl);
}

function decreaseYear()
{
    if (year == lastyear)
    {
        var yearinc: HTMLElement = document.getElementById("yearinc");
        activate(yearinc, "increaseYear()");
        setRightArrow(yearinc);
    }
    --year;
    if (year == firstyear)
    {
        var yeardec: HTMLElement = document.getElementById("yeardec");
        deactivate(yeardec);
        setLeftArrow(yeardec);
    }
    setYear();
}

function increaseYear()
{
    if (year == firstyear)
    {
        var yeardec: HTMLElement = document.getElementById("yeardec");
        activate(yeardec, "decreaseYear()");
        setLeftArrow(yeardec);
    }
    ++year;
    if (year == lastyear)
    {
        var yearinc: HTMLElement = document.getElementById("yearinc");
        deactivate(yearinc);
        setRightArrow(yearinc);
    }
    setYear();
}

